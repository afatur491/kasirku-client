$(document).ready(function () {
	var judul = $("#judul").text().toLowerCase();
	// $("#" + judul).addClass("active");

	const token = $("body").data("token");
	const url = $("#page-top").data("api");
	getuser();

	// add customer
	$(".add").click(function () {
		var email = $("#email").val();
		var pass = $("#pass").val();
		var username = $("#username").val();
		var datacus = {
			username: username,
			password: pass,
			email: email,
			level: "user",
		};
		addcus(datacus);
	});
	// mendapatkan data customer
	function getuser() {
		$.ajax({
			url: url + "customer",
			method: "get",
			dataType: "json",
			headers: {
				"X-Auth": token,
			},
			success: function (data) {
				var cus = data.data;
				let isi = "";
				$.each(cus, function (i, customer) {
					isi += isicus(customer, i + 1);
				});
				$(".bungkus").html(isi);
				$(".hapus").click(function () {
					var cusid = $(this).data("id");
					Swal.fire({
						title: "Are you sure?",
						text: "You won't be able to revert this!",
						icon: "warning",
						showCancelButton: true,
						confirmButtonColor: "#3085d6",
						cancelButtonColor: "#d33",
						confirmButtonText: "Yes, delete it!",
					}).then((result) => {
						if (result.value) {
							delcus(cusid);
						}
					});
				});
			},
		});
	}

	// isi tbody customer
	function isicus(cus, i) {
		return `<tr>
        <td>${i}</td>
        <td>${cus.username}</td>
        <td>${cus.email}</td>
        <td>${cus.point}</td>
        <td>
            <button class="btn btn-danger hapus" data-id="${cus.id}"><i class="fas fa-trash"></i></button>
        </td>
    </tr>`;
	}

	//fungsi menambah customer baru
	function addcus(cus) {
		$.ajax({
			url: url + "customer/add",
			method: "post",
			dataType: "json",
			headers: {
				"X-Auth": token,
			},
			data: cus,
			success: function (data) {
				// console.log(data);
				if (data.status == 1) {
					Swal.fire({
						icon: "success",
						title: "Success",
						text: data.pesan,
					});
					getuser();
					clear();
				} else {
					Swal.fire({
						icon: "error",
						title: "Failed",
						text: data.pesan,
					});
				}
			},
		});
	}
	// delete data customer

	function delcus(id) {
		$.ajax({
			url: url + "customer/delete",
			method: "delete",
			dataType: "json",
			headers: {
				"X-Auth": token,
			},
			data: {
				id: id,
			},
			success: function (data) {
				if (data.status == 1) {
					Swal.fire({
						icon: "success",
						title: "Success",
						text: data.pesan,
					});
					getuser();
					clear();
				} else {
					Swal.fire({
						icon: "error",
						title: "Failed",
						text: data.pesan,
					});
				}
				// console.log(data);
			},
		});
	}
	function clear() {
		$("#email").val("");
		$("#pass").val("");
		$("#username").val("");
	}
});
