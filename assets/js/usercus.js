$(document).ready(function () {
	function pindah(url) {
		window.location.href = url;
	}
	// alert("ok");
	$(".owl-carousel").owlCarousel({
		center: true,
		items: 2,
		loop: true,
		autoWidth: true,
		autoplay: true,
		dots: true,
		margin: 10,
		merge: true,
		nav: true,
		responsive: {
			0: {
				items: 1,
			},
			600: {
				items: 3,
			},
			1000: {
				items: 2,
			},
		},
	});
});
