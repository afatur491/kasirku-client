$(document).ready(function () {
	// const url = $("#page-top").data("api");
	const url = "http://fatur-api.oceancods.com/";

	// console.log(coba);
	$("#login").click(function () {
		var username = $("#username").val();
		var password = $("#password").val();
		if (username == "") {
			Swal.fire({
				icon: "error",
				title: "Failed",
				text: "Username Required",
			});
		} else if (password == "") {
			Swal.fire({
				icon: "error",
				title: "Failed",
				text: "Password Required",
			});
		} else {
			var data = {
				username: username,
				password: password,
			};
			login(data);
		}
	});

	function login(data) {
		$.ajax({
			url: url + "auth/login",
			method: "post",
			dataType: "json",
			data: data,
			success: function (data) {
				// console.log(data);
				if (data.status == 1) {
					Swal.fire({
						icon: "success",
						title: "Success",
						text: data.pesan,
					}).then(function (result) {
						if (result.value) {
							if (data.data.level == "admin") {
								window.location =
									"http://fatur-ui.oceancods.com/auth/confirm/" +
									data.data.token +
									"/" +
									data.data.level +
									"/" +
									data.data.id;
							} else {
								window.location =
									"http://fatur-ui.oceancods.com/auth/confirm/" +
									data.data.idadmin +
									"/" +
									data.data.level +
									"/" +
									data.data.id;
							}
						}
					});
				} else {
					Swal.fire({
						icon: "error",
						title: "Failed",
						text: data.pesan,
					});
				}
			},
		});
	}
});
