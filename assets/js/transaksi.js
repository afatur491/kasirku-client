$(document).ready(function () {
	const token = $("body").data("token");
	const url = $("#page-top").data("api");
	var produktrans = [];
	gettransaksi();
	// show username has
	$(".search").click(function () {
		var username = $(".key").val();
		if (username == "") {
			showerror("Username Required");
		} else {
			scustomer(username);
		}
	});
	// add disabled
	$(".key").keydown(function () {
		$(".checkout").attr("disabled", "disabled");
	});
	//only number
	$("#ttotal , #tprize, #moneypaid").keydown(function (e) {
		var key = e.charCode || e.keyCode || 0;
		return (
			key == 8 ||
			key == 9 ||
			key == 13 ||
			key == 46 ||
			key == 110 ||
			key == 190 ||
			(key >= 35 && key <= 40) ||
			(key >= 48 && key <= 57) ||
			(key >= 96 && key <= 105)
		);
	});
	// checkout button click
	$(".checkout").click(function () {
		if ($(".isitrans").data("jumlah") > 0) {
			$("#totalPay").val($(".total").text());
			$("#modalCheckout").modal("show");
		} else {
			showerror("The Cart is Empty");
		}
	});
	// gettransaksi
	function gettransaksi() {
		$.ajax({
			url: url + "transaksi",
			method: "get",
			dataType: "json",
			headers: {
				"X-Auth": token,
			},
			success: function (data) {
				var pri = data.data;
				let isi = "";
				$.each(pri, function (i, tran) {
					isi += isitran(tran, i + 1);
				});
				$(".trans").html(isi);
				// console.log(data.data[0]);
			},
		});
	}

	// cari customer
	function scustomer(username) {
		$.ajax({
			url: url + "customer/" + username,
			method: "get",
			dataType: "json",
			headers: {
				"X-Auth": token,
			},
			success: function (data) {
				if (data.status == 1) {
					$(".myname").text(data.data[0].username);
					$(".mypoint").text(data.data[0].point);
					$(".myemail").text(data.data[0].email);
					$("#form").modal("show");
					$(".checkout").removeAttr("disabled");
					$(".checkout").data("id", data.data[0].id);
				} else {
					Swal.fire({
						icon: "error",
						title: "Failed",
						text: data.pesan,
					});
					$(".checkout").attr("disabled", "disabled");
				}
			},
		});
	}
	// isi transaksi
	function isitran(data, num) {
		return `<tr>
		<td>${num}</td>
		<td>${data.nama}</td>
		<td>${data.jumlah}</td>
		<td>${data.total}</td>
		<td>${data.tgltrans}</td>
		<td>${data.username}</td>
	</tr>`;
	}
	// add to cart
	$(".addtrans").click(function () {
		if (
			$("#tname").val() == "" ||
			$("#ttotal").val() == "" ||
			$("#tprize").val() == ""
		) {
			showerror("Field Required");
		} else {
			data = {
				nama: $("#tname").val(),
				jumlah: $("#ttotal").val(),
				total: $("#tprize").val(),
			};
			$(".isitrans").append(isitrans(data));
			clear();
			hittotal("plus");
			produktrans.push(data);
			$(".cancel").click(function () {
				var nama = $(this).find("#nproduk").text();
				produktrans = $.grep(produktrans, function (e) {
					e.nama == nama;
				});
				$(this).closest("tr").remove();
				hittotal("min");
			});
			// checkout
			$(".pay").click(function () {
				if ($("#moneypaid").val() == "") {
					showerror("Money Paid Required");
				} else if ($("#changemoney").val() < 0) {
					showerror("Money Paid Less than Total");
				} else {
					addtrans(produktrans, $(".checkout").data("id"));
					Swal.fire({
						icon: "success",
						title: "Success",
						text: "Transaction Successfully",
					});
					$(".isitrans").html("");
					produktrans = [];

					clear();
					$(".total").text("0");
					$(".isitrans").data("jumlah", "0");
					$(".key").val("");
					$(".checkout").attr("disabled", "disabled");
					$("#tname").focus();
					$("#modalCheckout").modal("toggle");
				}
			});
		}
		// console.log(angka);
	});
	// isi carttransaksi
	function isitrans(data) {
		return ` <tr class="rowcancel">
        <td class="cancel" id="nproduk">${data.nama}</td>
        <td align="center" class="cancel" id="qproduk">${data.jumlah}</td>
        <td align="right" class="cancel jumlah" id="hproduk">${data.total}</td>
    </tr>`;
	}
	// function clear
	function clear() {
		$("#tname").val("");
		$("#ttotal").val("");
		$("#tprize").val("");
		$("#moneypaid").val("");
		$("#changemoney").val("");
	}
	// function hitung total
	function hittotal(arit) {
		let angka = 0;
		$(".jumlah").each(function () {
			if (!isNaN($(this).text()) && $(this).text().lenght != 0) {
				angka += parseFloat($(this).text());
			}
		});
		$(".total").text(angka);
		$("#tname").focus();
		if (arit == "min") {
			$(".isitrans").data("jumlah", $(".isitrans tr").length);
		} else if (arit == "plus") {
			$(".isitrans").data("jumlah", $(".isitrans tr").length);
		}
		console.log($(".isitrans").data("jumlah"));
	}
	// function untuk menampilkan error
	function showerror(title) {
		const Toast = Swal.mixin({
			toast: true,
			position: "top-end",
			showConfirmButton: false,
			timer: 3000,
			timerProgressBar: true,
			onOpen: (toast) => {
				toast.addEventListener("mouseenter", Swal.stopTimer);
				toast.addEventListener("mouseleave", Swal.resumeTimer);
			},
		});

		Toast.fire({
			icon: "error",
			title: title,
		});
	}
	// kembalian on change
	$("#moneypaid").keyup(function () {
		// $("#moneyPaid").val() - $("#totalPay").val();
		if ($(this).val() == "") {
			$("#changemoney").val(parseFloat(0 - $("#totalPay").val()));
		} else {
			$("#changemoney").val($(this).val() - $("#totalPay").val());
		}
	});

	// add transaksi fungsi
	function addtrans(data, id) {
		for (let i = 0; i < data.length; i++) {
			$.ajax({
				url: url + "transaksi/add",
				method: "post",
				dataType: "json",
				headers: {
					"X-Auth": token,
				},
				data: {
					iduser: id,
					nama: data[i].nama,
					jumlah: data[i].jumlah,
					total: data[i].total,
				},
				success: function (data) {},
			});
		}
	}
});
