$(document).ready(function () {
	const token = $("body").data("token");
	const url = $("#page-top").data("api");
	const myurl = $("#page-top").data("url");
	getprize();

	// ganti gambar
	$(".newgambar").click(function () {
		$(".gambar").click();
	});
	$(".gambar").change(function () {
		if (this.files && this.files[0]) {
			var reader = new FileReader();

			reader.onload = function (e) {
				$(".newgambar").attr("src", e.target.result);
				$(".newgambar").data("gambar", e.target.result);
			};

			reader.readAsDataURL(this.files[0]); // convert to base64 string
		}
	});

	$(".addprize").click(function () {
		var reward = $("#reward").val();
		var point = $("#point").val();
		var gambar = $(".newgambar").data("gambar");
		var data = {
			point: point,
			reward: reward,
			gambar: gambar,
		};
		addprize(data);
	});
	// fungsi untuk addprize
	function addprize(data) {
		$.ajax({
			url: url + "transaksi/addprize",
			method: "post",
			dataType: "json",
			headers: {
				"X-Auth": token,
			},
			data: data,
			success: function (data) {
				if (data.status == 1) {
					Swal.fire({
						icon: "success",
						title: "Success",
						text: data.pesan,
					});
					$(".isiprize").html("");
					getprize();
					clear(myurl);
				} else {
					Swal.fire({
						icon: "error",
						title: "Failed",
						text: data.pesan,
					});
				}
			},
		});
	}
	function getprize() {
		$.ajax({
			url: url + "transaksi/getprize",
			method: "get",
			dataType: "json",
			headers: {
				"X-Auth": token,
			},
			success: function (data) {
				var pri = data.data;
				let isi = "";
				$.each(pri.reverse(), function (i, prize) {
					isi = isiprize(prize, url);
					$(".isiprize").append(isi);
				});
				$(".hapus").click(function () {
					var id = $(this).data("id");
					Swal.fire({
						title: "Are you sure?",
						text: "You won't be able to revert this!",
						icon: "warning",
						showCancelButton: true,
						confirmButtonColor: "#3085d6",
						cancelButtonColor: "#d33",
						confirmButtonText: "Yes, delete it!",
					}).then((result) => {
						if (result.value) {
							hapus(id);
						}
					});
				});
				$(".ubah").click(function () {
					var id = $(this).data("id");
					getprizebyid(id);
				});
			},
		});
	}

	// fungsi mengambil hadiah berdasarkan id

	function getprizebyid(id) {
		$.ajax({
			url: url + "transaksi/getprize/" + id,
			method: "get",
			dataType: "json",
			headers: {
				"X-Auth": token,
			},
			success: function (data) {
				var pri = data.data[0];
				$("#reward").val(pri.reward);
				$("#point").val(pri.point);
				$(".newgambar").attr("src", url + pri.gambar);
			},
		});
	}

	function clear(url) {
		$("#reward").val("");
		$("#point").val("");
		$(".newgambar").attr("src", url + "assets/img/buy.png");
		$(".gambar").val("");
	}

	// fungsi untuk menghapus hadiah

	function hapus(id) {
		$.ajax({
			url: url + "transaksi/deleteprize",
			method: "delete",
			dataType: "json",
			headers: {
				"X-Auth": token,
			},
			data: {
				id: id,
			},
			success: function (data) {
				if (data.status == 1) {
					Swal.fire({
						icon: "success",
						title: "Success",
						text: data.pesan,
					});
					$(".isiprize").html("");
					getprize();
				} else {
					Swal.fire({
						icon: "error",
						title: "Failed",
						text: data.pesan,
					});
				}
			},
		});
	}

	function isiprize(prize, url) {
		return `<div class="col-lg-6 col-md-6 col-sm-12 ">
		<div class="card mb-3 shadow" style="max-width: 540px;">
			<div class="row no-gutters">
				<div class="col-md-4">
					<img src="${url + prize.gambar}" class="card-img" alt="...">
				</div>
				<div class="col-md-8">
					<div class="card-body">
						<h5 class="card-title">${prize.reward}</h5>
						<h2 class="card-text">${prize.point} Point</h2>
						<p class="card-text">
						<a href="#" class="badge badge-danger hapus" data-id="${
							prize.id
						}"><i class="fas fa-trash"></i></a>
						</p>			
					</div>
				</div>
			</div>
		</div>
	</div>`;
	}
});
