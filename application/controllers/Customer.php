<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Customer extends CI_Controller
{
    public $bio;
    public function __construct()
    {
        parent::__construct();
        if (!$this->session->has_userdata('token')) {
            redirect(base_url('auth/login'));
        } else {
            if ($this->session->userdata('level') != "user") {
                redirect(base_url('home'));
            }
            $this->bio = $this->Get_Model->getdata($this->session->userdata('id'), $this->session->userdata('token'));
        }
    }
    public function index()
    {
        $data['nama'] = $this->bio["username"];
        $data['point'] = $this->bio["point"];
        $data['hadiah'] = $this->Get_Model->gethadiah($data['point'], $this->session->userdata('token'));
        $data['transaksi'] =  $this->Get_Model->gethistory($this->session->userdata('id'), "transaksi", $this->session->userdata('token'));
        $data['title'] = "Kasirku | Customer";
        $this->load->view('template/top', $data);
        $this->load->view('customer/index', $data);
        $this->load->view('template/bottom');
        // var_dump($this->bio);
    }
    public function history()
    {
        $data['nama'] = $this->bio->username;
        $data['reward'] =  $this->Get_Model->gethistory($this->session->userdata('id'), "hadiah", $this->session->userdata('token'));
        $data['transaksi'] =  $this->Get_Model->gethistory($this->session->userdata('id'), "transaksi", $this->session->userdata('token'));
        $data['title'] = "Kasirku | History";
        $this->load->view('template/top', $data);
        $this->load->view('customer/history', $data);
        $this->load->view('template/bottom');
    }
    public function tukar($idh)
    {
        $this->Get_Model->tukar($this->session->userdata('id'), $idh, $this->session->userdata('token'));
        $this->session->set_flashdata('tukar', 'sukses');
        redirect(base_url('customer'));
    }
    public function account()
    {
        $data['id'] = $this->session->userdata('id');
        $data['username'] = $this->input->post('username');
        $data['email'] = $this->input->post('email');
        $data['password'] = $this->input->post('password');
        $data['oldpass'] = $this->input->post('oldpass');
        $this->form_validation->set_rules('username', 'Userrname', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('password', 'password', 'required');
        $this->form_validation->set_rules('oldpadd', 'Old Password', 'required');
        if ($this->form_validation->run() == FALSE) {
            $data['nama'] = $this->bio->username;
            $data['email'] = $this->bio->email;
            $data['title'] = "Kasirku | Account";
            $this->load->view('template/top', $data);
            $this->load->view('customer/account', $data);
            $this->load->view('template/bottom');
        } else {
            $edit = $this->Get_Model->changeakun($data, $this->session->userdata('token'));
            if ($edit['status'] == 0) {
                $this->session->set_flashdata('sukses', $edit['pesan']);
                redirect(base_url('customer/account'));
            } else {
                $this->session->set_flashdata('gagal', $edit['pesan']);
                redirect(base_url('customer/account'));
            }
        }
    }
}