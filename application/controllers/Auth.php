<?php

class Auth extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    public function login()
    {
        if ($this->session->has_userdata('token')) {
            if ($this->session->userdata('level') == "user") {
                redirect(base_url('customer'));
            } elseif ($this->session->userdata('level') == "admin") {
                redirect(base_url('home'));
            }
        }
        $this->load->view('login');
        // $this->load->view('template/bottom');
    }
    public function logout()
    {
        $this->session->unset_userdata(array("token", "level", "id"));
        redirect(base_url('auth/login'));
    }
    public function confirm($token, $level, $id)
    {
        if ($token == "" || $level == "" || $id == "") {
            redirect(base_url('auth/login'));
        } else {
            if ($level == "user") {
                $this->session->set_userdata(array("token" => $this->Get_Model->getToken($token)->token, "level" => $level, "id" => $id));
                redirect(base_url('customer'));
            } elseif ($level == "admin") {
                $this->session->set_userdata(array("token" => $token, "level" => $level, "id" => $id));
                redirect(base_url('home'));
            }
        }
    }
}