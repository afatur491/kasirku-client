<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		if (!$this->session->has_userdata('token')) {
			redirect(base_url('auth/login'));
		} else {
			if ($this->session->userdata('level') != "admin") {
				redirect(base_url('customer'));
			}
		}
	}
	public function index()
	{
		$data['title']	= "Kasirku | Admin Dashboard";
		$data['nama']	= $this->Get_Model->getToken($this->session->userdata('id'))->username;
		$this->load->view('template/top', $data);
		$this->load->view('template/header', $data);
		$this->load->view('home/index');
		$this->load->view('template/footer');
		$this->load->view('template/bottom');
	}
	public function customer()
	{
		$data['nama']	= $this->Get_Model->getToken($this->session->userdata('id'))->username;
		$data['title'] = "Kasirku | Admin Customer";
		$this->load->view('template/top', $data);
		$this->load->view('template/header', $data);
		$this->load->view('users/index');
		$this->load->view('template/footer');
		$this->load->view('template/bottom');
	}
	public function transaction()
	{
		$data['nama']	= $this->Get_Model->getToken($this->session->userdata('id'))->username;
		$data['title'] = "Kasirku | Admin Transaction";
		$this->load->view('template/top', $data);
		$this->load->view('template/header', $data);
		$this->load->view('transaction/index');
		$this->load->view('template/footer');
		$this->load->view('template/bottom');
	}
	public function prize()
	{
		$data['nama']	= $this->Get_Model->getToken($this->session->userdata('id'))->username;
		$data['title'] = "Kasirku | Admin Prize";
		$this->load->view('template/top', $data);
		$this->load->view('template/header', $data);
		$this->load->view('prize/index');
		$this->load->view('template/footer');
		$this->load->view('template/bottom');
	}
}