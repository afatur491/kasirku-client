<nav class="navbar navbar-expand navbar-light bg-nav topbar static-top">
    <div class="container">
        <a class="navbar-brand text-white" href="<?= base_url('customer') ?>">Kasirku</a>
        <!-- Topbar Navbar -->
        <ul class="navbar-nav ml-auto">
            <li class="nav-item dropdown no-arrow">
                <a class="nav-link dropdown-toggle" href="<?= base_url('auth/logout') ?>">
                    <span class="mr-2 d-lg-inline text-white small btn bg-gradient"><i class="fas fa-sign-out-alt"></i>
                        Logout</span>
                </a>
            </li>
        </ul>
    </div>
</nav>
<div class="jumbotron jumbotron-fluid">
    <div class="container">

    </div>
</div>
<div class="container mypage">
    <div class="row justify-content-center">
        <div class="col-lg-10">
            <div class="row align-middle">
                <div class="col-lg-12">
                    <h2 class="card-title inisial text-white">
                        <a href="<?= base_url('customer/account') ?>" class="text-white">
                            <?= $nama ?> <span class="kecil"><i class="fas fa-edit"></i></span>
                        </a>
                    </h2>
                </div>
                <div class="col-lg-12 mb-2">
                    <h5 class="card-text text-white">
                        <span class="px-2 rounded-pill bg-gra text-center"><?= $point ?> <i
                                class="fab fa-bitcoin"></i></span>
                    </h5>
                </div>
            </div>
            <div class="card shadow ">
                <div class="row">
                    <div class="card-body col-lg-12">
                        <div class="container">
                            <div class="row align-middle">
                                <div class="col-lg-12">
                                    <h3 class="card-title judul text-center">
                                        Today's Shopping Cart
                                    </h3>
                                </div>
                                <div class="col-lg-12">
                                    <h2 class="card-text">
                                        <i class="fas fa-shopping-basket"></i> <span class="section-title">
                                            <?php
                                            echo array_count_values(array_column($transaksi, "tgltrans"))[date("d F Y")] ?? '0';
                                            ?>
                                        </span>
                                    </h2>
                                </div>
                                <div class="col-lg-12 text-right">
                                    <a href="<?= base_url('customer/history') ?>" class="btn-his text-white">
                                        History</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<section class="py-2">
    <div class="container mt-4">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="text-center section-title ">Gifts that can be exchanged</h3>
            </div>
        </div>
    </div>
    <div class="owl-carousel owl-theme row mt-4">
        <?php foreach ($hadiah as $hdh) { ?>
        <div class="col-lg-6 col-md-6 col-sm-12 item" data-merge="1">
            <div class="card mb-3 shadow" style="width: 500px;">
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="http://localhost/kasirku-server/<?= $hdh['gambar']; ?>" class="card-img" alt="...">
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h5 class="card-title text-center judul"><?= $hdh['reward']; ?></h5>
                            <h2 class="card-text text-card"><?= $hdh['point']; ?> <span><i
                                        class="fab fa-bitcoin"></i></span></h2>
                            <p class="text-right">
                                <a href="<?= base_url('customer/tukar/') ?><?= $hdh['id']; ?>"
                                    class="btn-his text-white card-text tukar" data-id="<?= $hdh['id']; ?>">Exchange</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
</section>
<?php if ($this->session->flashdata('tukar')) { ?>
<script>
Swal.fire({
    icon: "success",
    title: "Success",
    text: "Point Exhange",
});
</script>
<?php } ?>