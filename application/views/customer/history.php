<nav class="navbar navbar-expand navbar-light bg-nav2 topbar static-top">
    <div class="container">
        <a class="navbar-brand text-white" href="<?= base_url('customer') ?>">Kasirku</a>
        <ul class="navbar-nav ml-auto">
            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
                <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                    <span class="mr-2 d-none d-lg-inline small text-white"><?= $nama ?></span>
                </a>
                <!-- Dropdown - User Information -->
                <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                    <a class="dropdown-item" href="<?= base_url('auth/logout') ?>">
                        <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                        Logout
                    </a>
                </div>
            </li>

        </ul>
    </div>
</nav>
<div class="container">
    <div class="row">
        <div class="col-lg-12 my-4">
            <h3 class="section-title-history">History</h3>
        </div>
        <div class="col-lg-12">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" href="#profile" role="tab" data-toggle="tab">Transaction</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#buzz" role="tab" data-toggle="tab">Reward</a>
                </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade in show active" id="profile">
                    <div class="row mt-4">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Product Name</th>
                                            <th>Quantity</th>
                                            <th>Price</th>
                                            <th>Transaction Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i = 1;
                                        foreach ($transaksi as $trs) { ?>
                                        <tr>
                                            <td><?= $i++ ?></td>
                                            <td><?= $trs['nama'] ?></td>
                                            <td><?= $trs['jumlah'] ?></td>
                                            <td>IDR <?php echo number_format($trs['total'], 2, ",", ".")  ?></td>
                                            <td>
                                                <?php
                                                    if ($trs['tgltrans'] == date("d F Y")) {
                                                        echo "Today";
                                                    } else {
                                                        echo $trs['tgltrans'];
                                                    }
                                                    ?>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="buzz">
                    <div class="row mt-4">
                        <?php foreach ($reward as $rwd) { ?>
                        <div class="col-lg-6">
                            <div class="card mb-3 shadow" style="max-width: 500px;">
                                <div class="row no-gutters">
                                    <div class="col-md-4">
                                        <img src="http://localhost/kasirku-server/<?= $rwd['gambar'] ?>"
                                            class="card-img" alt="...">
                                    </div>
                                    <div class="col-md-8">
                                        <div class="card-body">
                                            <h5 class="card-title text-center judul"><?= $rwd['reward'] ?></h5>
                                            <h2 class="card-text text-card"><?= $rwd['point'] ?> <span><i
                                                        class="fab fa-bitcoin"></i></span></h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>