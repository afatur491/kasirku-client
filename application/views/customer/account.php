<nav class="navbar navbar-expand navbar-light bg-nav2 topbar static-top">
    <div class="container">
        <a class="navbar-brand text-white" href="<?= base_url('customer') ?>">Kasirku</a>
        <ul class="navbar-nav ml-auto">
            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
                <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                    <span class="mr-2 d-none d-lg-inline small text-white"><?= $nama ?></span>
                </a>
                <!-- Dropdown - User Information -->
                <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                    <a class="dropdown-item" href="<?= base_url('auth/logout') ?>">
                        <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                        Logout
                    </a>
                </div>
            </li>

        </ul>
    </div>
</nav>
<div class="container">
    <div class="row">
        <div class="col-lg-12 my-4">
            <h3 class="section-title-history">Account</h3>
        </div>
        <div class="col-lg-12">
            <form action="" method="post">
                <div class="form-group">
                    <label for="username">Username</label>
                    <input type="email" class="form-control" id="username" name="username" value="<?= $nama ?>">
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" id="email" name="email" value="<?= $email ?>">
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password" name="password">
                </div>
                <div class="form-group">
                    <label for="oldpass">Old Password</label>
                    <input type="password" class="form-control" id="oldpass" name="oldpass">
                </div>
                <button type="submit" name="change" class="btn btn-primary btn-block">Change</button>
            </form>
        </div>
    </div>
</div>
<?php if ($this->session->flashdata('sukses')) { ?>
<script>
Swal.fire({
    icon: "success",
    title: "Success",
    text: "Account Changed",
});
</script>
<?php } ?>
<?php if ($this->session->flashdata('gagal')) { ?>
<script>
Swal.fire({
    icon: "success",
    title: "Success",
    text: "Failed Changed",
});
</script>
<?php } ?>