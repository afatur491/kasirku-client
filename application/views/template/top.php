<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?= $title ?></title>

    <!-- Custom fonts for this template-->
    <link href="<?= base_url('assets/') ?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->

    <link href="<?= base_url('assets/') ?>css/sb-admin-2.min.css" rel="stylesheet">
    <link href="<?= base_url('assets/') ?>vendor/owl-corousel/css/owl.carousel.min.css" rel="stylesheet">
    <link href="<?= base_url('assets/') ?>vendor/owl-corousel/css/owl.theme.default.min.css" rel="stylesheet">
    <link href="<?= base_url('assets/') ?>vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="<?= base_url('assets/') ?>css/me.css" rel="stylesheet">

</head>

<body id="page-top" data-url='<?= base_url() ?>' data-token="<?= $this->session->userdata('token') ?>"
    data-api="http://fatur-api.oceancods.com/" data-coba="coba">
    <script src="<?= base_url() ?>assets/js/sweet-alert.js"></script>