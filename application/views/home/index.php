<div class="container-fluid">
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800" id="judul">Dashboard</h1>
    </div>

    <!-- Content Row -->
    <div class="row">
        <!-- star input -->
        <div class="col-lg-8 mb-2">
            <div class="card shadow card-search">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <!-- <label for="tname">Product Name</label> -->
                                <input type="text" class="form-control" id="tname" placeholder="Product Name">
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <!-- <label for="ttotal">Product Quantity</label> -->
                                <input type="number" class="form-control" id="ttotal" placeholder="Quantity" min="0">
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <!-- <label for="tprize">Product Prize</label> -->
                                <input type="text" class="form-control" id="tprize" placeholder="Price">
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group">
                                <button class="btn btn-danger btn-block addtrans"><i class="fas fa-plus"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end input -->
        <div class="col-lg-4">
            <div class="card shadow card-search mb-2">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" class="form-control key" placeholder="Customer Username"
                                        data-id="">
                                    <div class="input-group-append">
                                        <button class="btn btn-primary search" type="button"><i
                                                class="fas fa-search"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card shadow mb-4">
                <div class="card-body isicheckout">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th class="text-left">Name</th>
                                <th class="text-center">Qty</th>
                                <th class="text-right">Prize</th>
                            </tr>
                        </thead>
                        <tbody class="isitrans" data-jumlah="0">

                        </tbody>
                    </table>
                </div>
                <div class="card-footer text-right">
                    <small class="text-black">Total</small>
                    <h2 class="total text-warning">0</h2>
                    <button class="btn btn-warning checkout" disabled data-id="">Checkout</button>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- modal konfirmasi -->
<div class="modal fade" id="form" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card shadow">
                            <div class="row">
                                <div class="card-body col-lg-12">
                                    <div class="container">
                                        <div class="row align-middle">
                                            <div class="col-lg-12">
                                                <h2 class="card-title myname">

                                                </h2>
                                                <h6 class="card-subtitle mb-2 text-muted myemail">
                                                </h6>
                                            </div>
                                            <div class="col-lg-9">
                                                <h5 class="card-text">
                                                    <span
                                                        class="px-3 rounded-pill bg-gradient text-white text-center mypoint"></span>
                                                    Point
                                                </h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary has" data-dismiss="modal" data-id="">Select</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalCheckout" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12 text-center text-uppercase">
                        <h2>Checkout</h2>
                    </div>
                    <div class="col-lg-12">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fas fa-money-bill-alt"></i>
                                </span>
                            </div>
                            <input type="text" class="form-control" placeholder="Total Pay" disabled id="totalPay">
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fas fa-money-check-alt"></i>
                                </span>
                            </div>
                            <input type="text" class="form-control" placeholder="Money Paid" id="moneypaid">
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fas fa-hand-holding-usd"></i>
                                </span>
                            </div>
                            <input type="text" class="form-control" placeholder="Change Money" id="changemoney"
                                disabled>
                        </div>
                    </div>
                    <div class="col-lg-12 text-right">
                        <button type="button" class="btn btn-primary pay"><i class="fas fa-dollar-sign"></i>
                            <b>Pay</b></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<!-- End of Main Content -->