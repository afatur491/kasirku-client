<div class="container-fluid">
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800" id="judul">Prize</h1>
        <a href="#" id='addPrize' class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" data-toggle="modal"
            data-target="#form" data-jenis='new'><i class="fas fa-award text-white-50 mr-2"></i>Add New Prize</a>
    </div>

    <!-- Content Row -->
    <div class="row isiprize">

    </div>
</div>
</div>



<div class="modal fade" id="form" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header biru">
                <h5 class="modal-title">Add New Prize</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 ">
                        <div class="card mb-3 shadow">
                            <div class="row no-gutters">
                                <div class="col-lg-4">
                                    <img src="<?= base_url('assets/img') ?>/buy.png" class="card-img newgambar"
                                        style="cursor:pointer;" data-gambar="">
                                    <input type="file" class="gambar" style="display: none;">
                                </div>
                                <div class="col-lg-8">
                                    <div class="card-body">
                                        <h5 class="card-title"><input class="form-control" type="text"
                                                placeholder="Reward" id="reward"></h5>
                                        <h2 class="card-text">
                                            <div class="input-group mb-3">
                                                <input type="text" class="form-control" placeholder="20" id="point">
                                                <div class="input-group-append">
                                                    <span class="input-group-text">Point</span>
                                                </div>
                                            </div>
                                        </h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary addprize" data-id=""><i class="fas fa-plus"></i>
                    Add</button>
            </div>
        </div>
    </div>
</div>
<!-- End of Main Content -->